# Using Institute runners with unlimited minutes

The tango-controls group provides several CI runners. Those runners also don't
count against the gitlab CI minutes quota.

Steps:
- Enable group runners in the project CI/CD settings (under Runners)
- Add the required [tags](https://docs.gitlab.com/ee/ci/yaml/#tags) to your
  pipeline jobs. For the common case of a linux docker job set the following
  tags for each job in your `.gitlab-ci.yaml` file.

  ```
  tags:
  - [docker, linux, amd64]
  ```

- For Docker-in-Docker use the following in your job together with the
  `dind` tag:

  ```
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  ```

# Runner tags naming scheme

Architecture:
- `amd64`/`aarch64`

OS:
- `windows`/`linux`/`macosx`

OS subtype for `macosx`:
- `catalina`/`bigsur`/`monterey`/`ventura`

Runner type:
- `docker`/`shell`

Runner subtype for `docker`:
- `dind`

# Available runners

We have the following runners available:
- `amd64`, `linux`, `docker`
- `amd64`, `linux`, `docker`, `dind`
- `aarch64`, `linux`, `docker`
- `aarch64`, `linux`, `docker`, `dind`

The following institutes are currently providing runners:

- `skao` (@tjuerges)
- `maxiv` (@beenje)
- `alba` (@guifrecuni)
- `desy` (@yamatveyev)

In case of an issue with one of the runners, please create a ticket at
https://gitlab.com/tango-controls/TangoTickets and assign it to the correct
contact person (see above).

# Adding a new runner

The required machine specifications are 2 vCPUs, 8GB of RAM and 50GB disk space
*per* concurrent job.

Steps:
- Get a VM
- Install the runner on the VM
- Add a docker registry mirror, see
  [here](https://about.gitlab.com/blog/2020/10/30/mitigating-the-impact-of-docker-hub-pull-requests-limits) for plain `docker` runners and
   [here](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#enable-registry-mirror-for-dockerdind-service) for `dind`.
- Add the runner tags as described above, Also add an institute tag so that everyone knows
  which runners belong to whom.
- Request maintainer access to https://gitlab.com/tango-controls/test-institute-runners
- Add your runner there
- Test it
- Once it works, request your local gitlab owner to get them added as group
  runners to gitlab.com/tango-controls
- Add a merge request against this repository to add your institute and
  responsible support person
